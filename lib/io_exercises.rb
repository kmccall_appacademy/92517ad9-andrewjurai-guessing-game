# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.

# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.


  def guessing_game
    random = rand(1..100)
    print "Guess a number between 1 and 100\n"
    guess = gets.chomp.to_i
    amt_guesses = 1

    until guess == random
      print "You guessed #{guess}\n"
      print too_high_or_too_low(guess, random)
      print "Guess a next number\n"
      guess = gets.chomp.to_i
      amt_guesses += 1
    end

    conclude(amt_guesses, random)


  end

  def conclude(amt_guesses,random)
    print "Lucky Guess! How'd you know it was #{random}"
    print "It took you #{amt_guesses} guesses!"
  end

  def too_high_or_too_low(guess, random)

    return "too high\n" if guess > random
    return "too low\n" if guess < random
  end
